#+title: NixOS Notes

* Official & Semi-Official Resources
** NixOS Manual
https://nixos.org/nixos/manual
** NixPkgs Manual
https://nixos.org/nixpkgs/manual
** Nix Manual
https://nixos.org/nix/manual
** NixOS Weekly
Official newsletter that includes a lot of useful links found around
the web.

https://weekly.nixos.org/
** Nix.dev
Unofficial resource for developers using NixOS.

https://nix.dev/
** Tweag.io Blog
Blog by the company that made Lorri package for Nix.

https://www.tweag.io/blog/
** Planet NixOS

https://planet.nixos.org/

* Helpful Links
These are helpful links that have helped me understand Nix and NixOS.

** NixCloud.io

https://nixcloud.io/

** Converting Cabal to Nix

https://mmhaskell.com/blog/2020/2/10/converting-cabal-to-nix

** Homelab (using NixOS)

https://thewagner.net/blog/2020/05/31/homelab/

** Nix Wireless AP
Random gist I found.

https://gist.githubusercontent.com/Zer0-/16129d84a41fbb7ae27d/raw/ab0d721df19e9b143f6ce2b43b48d143ea8fe7ec/networking.nix

** Just Enough Nix for Programming

https://lambdablob.com/series/just-enough-nix-for-programming/
** Nix and Haskell in Production (Gabriel Gonzalez)
This is a much referenced git repo where Gabriel explains his process
of using Nix to manage Haskell packages instead of Stack.

https://github.com/Gabriel439/haskell-nix
** Setting Up a Haskell Dev Env in NixOS

https://romainviallard.dev/blog/setting-up-a-haskell-development-environment-with-nix/
** Nix by Example (excellent)
Excellent blog post that goes the Nix expression language in
wonderful, accessible detail.

https://jameshfisher.com/2014/09/28/nix-by-example/

** Is there much difference between using nix-shell and Docker?
Illuminating discussion of the titular question.
https://discourse.nixos.org/t/is-there-much-difference-between-using-nix-shell-and-docker-for-local-development/807/4
** My NixOS Desktop Flow (Christine Dodrill)
Extensive blog post by a software developer. Lots of good, clear, slow
explanations of core concepts in NixOS.

https://christine.website/blog/nixos-desktop-flow-2020-04-25
** Migrating to NixOS
2019. Extensive blog post

https://www.malloc47.com/migrating-to-nixos/
** Github Strategies for configuration.nix?
Good discussion of how to keep your configuration under version
control.

https://discourse.nixos.org/t/github-strategies-for-configuration-nix/1983/19
** Your home in Nix (dotfile management)
Helpful post for inspiration, though parts are somewhat confusing.
Links to a repo with examples.

https://hugoreeves.com/posts/2019/nix-home/
** About using Nix in my development workflow

https://ejpcmac.net/blog/about-using-nix-in-my-development-workflow/
** 4 Years with Literate Configuration

https://www.alexeyshmalko.com/2020/literate-config/
** Everything I Know (About Nix)
Interesting public wiki with lots of great (but unanotated) links.

https://wiki.nikitavoloboev.xyz/package-managers/nix/
** Nix and NixOS Workshops
Nice introductory workshops by Geoff Huntley (see below).

https://github.com/ghuntley/workshops
** NixOS Dotfiles from Geoff Huntley
Extremely comprehensive example of dotfiles from a real expert in
NixOS.

https://github.com/ghuntley/dotfiles-nixos
** Lorri
An alternative to Nix-shell. Supposedly better. Look into it later.

https://github.com/target/lorri
** A Gentle Introduction to the Nix Family
August 2020

Very interesting and comprehensive guide. Seems like it was written as
a series of notes that they wrote while they were learning how to use
Nix. This blog post is written under the assumption that the goal is
to install and use NixOS.

https://ebzzry.io/en/nix/
** Isolated environments with nix-shell and zsh (2020)
Apr 2020

[[https://msitko.pl/blog/2020/04/22/isolated-ennvironments-with-nix-shell-and-zsh.html]]

* Essential Programs
** Home-Manager
Allows you to use Nix configuration for your home directory. No
possible counterpart in the default NixOS.
** Dir-Env
** Lorri
** NIV
** Cachix
* User Configs
These are user configuration files that I find on the internet that
have (or will) help me fine tune my Nix config.
** Robert Helgesson (Rycee)
This is the guy who made Home-Manager for Nix. Here is his Emacs.nix
file:

https://gitlab.com/rycee/configurations/blob/master/user/emacs.nix
** ElvishJerricco
A user on Reddit that has posted his config files. I can't find them
right now but I will set the correct link when I do.

https://www.reddit.com/user/ElvishJerricco/
* Lambda Calculus
** The Lambda Calculus for Absolute Dummies (like myself)

http://palmstroem.blogspot.com/2012/05/lambda-calculus-for-absolute-dummies.html

