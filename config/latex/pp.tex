\chapter{A Bohemian in Exile}

\midaligned{\sc A Reminiscence}
\blank

\placefigure[left]{none}{\externalfigure[w20]}

\strut {\sc hen,} many years ago now, the once potent and extensive kingdom of
Bohemia gradually dissolved and passed away, not a few historians were
found to chronicle its past glories; and some have gone on to tell the
fate of this or that once powerful chieftain who either donned the
swallow-tail and conformed or, proudly self-exiled, sought some quiet
retreat and died as he had lived, a Bohemian. But these were of the
princes of the land. To the people, the villeins, the common rank and
file, does no interest attach? Did they waste and pine, an{\ae}mic, in
thin, strange, unwonted air? Or sit at the table of the scornful and
learn, with Dante, how salt was alien bread? It is of one of those
faithful commons I would speak, narrating only \quote{the short and
simple annals of the poor.}

It is to be noted that the kingdom aforesaid was not so much a kingdom
as a United States~-- a collection of self-ruling guilds,
municipalities, or republics, bound together by a common method of
viewing life. \quote{There {\em once} was a king of Bohemia}~-- but
that was a long time ago, and even Corporal Trim was not certain in
whose reign it was. These small free States, then, broke up gradually,
from various causes and with varying speed; and I think ours was one
of the last to go.

With us, as with many others, it was a case of lost
leaders. \quote{Just for a handful of silver he left us}; though it
was not exactly that, but rather that, having got the handful of
silver, they wanted a wider horizon to fling it about under than
Bloomsbury afforded.

\blank
\startnarrower
So they left us for their pleasure; and in due time,
one by one~-- 
\stopnarrower
\blank

But I will not be morose about them; they had honestly earned their
success, and we all honestly rejoiced at it, and do so still.

When old Pan was dead and Apollo's bow broken, there were many
faithful pagans who would worship at no new shrines, but went out to
the hills and caves, truer to the old gods in their discrowned
desolation than in their pomp and power. Even so were we left behind,
a remnant of the faithful. We had never expected to become great in art
or song; it was the life itself that we loved; that was our end~-- not,
as with them, the means to an end.

\startnarrower
\startlines
We aimed at no glory, no lovers of glory we;
Give us the glory of going on and still to be.
\stoplines
\stopnarrower

Unfortunately, going on was no longer possible; the old order had
changed, and we could only patch up our broken lives as best might be.

Fothergill said that he, for one, would have no more of it. The past
was dead, and he wasn't going to try to revive it. Henceforth he,
too, would be dead to Bloomsbury. Our forefathers, speaking of a man's
death, said \quote{he changed his life.} This is how Fothergill
changed his life and died to Bloomsbury. One morning he made his way
to the Whitechapel Road, and there he bought a barrow. The
Whitechapel barrows are of all sizes, from the barrow wheeled about by
a boy with half a dozen heads of cabbages to barrows drawn by a tall
pony, such as on Sundays take the members of a club to Epping
Forest. They are all precisely the same in plan and construction, only
in the larger sizes the handles develop or evolve into shafts; and
they are equally suitable, according to size, for the vending of
whelks, for a hot-potato can, a piano organ, or for the conveyance of
a cheery and numerous party to the Derby. Fothergill bought a medium
sized \quote{developed} one, and also a donkey to fit; he had it
painted white, picked out with green~-- the barrow, not the
donkey~-- and when his arrangements were complete, stabled the whole
for the night in Bloomsbury. The following morning, before the early
red had quite faded from the sky, the exodus took place, those of us
who were left being assembled to drink a parting whisky-and-milk in
sad and solemn silence. Fothergill turned down Oxford Street, sitting
on the shaft with a short clay in his mouth, and disappeared from our
sight, heading west at a leisurely pace. So he passed out of our lives
by way of the Bayswater Road.

They must have wandered far and seen many things, he and his donkey,
from the fitful fragments of news that now and again reached us. It
seems that eventually, his style of living being economical, he was
enabled to put down his donkey and barrow, and set up a cart and a
mare~-- no fashionable gipsy-cart, a sort of houseboat on wheels, but a
light and serviceable cart, with a moveable tilt, constructed on his
own designs. This allowed him to take along with him a few canvases
and other artists' materials; soda-water, whisky, and such like
necessaries; and even to ask a friend from town for a day or two, if
he wanted to.

He was in this state of comparative luxury when at last, by the merest
accident, I foregathered with him once more. I had pulled up to
Streatley one afternoon, and, leaving my boat, had gone for a long
ramble on the glorious North Berkshire Downs to stretch my legs before
dinner. Somewhere over on Cuckhamsley Hill, by the side of the
Ridgeway, remote from the habitable world, I found him, smoking his
vesper pipe on the shaft of his cart, the mare cropping the short
grass beside him. He greeted me without surprise or effusion, as if
we had only parted yesterday, and without a hint of an allusion to
past times, but drifted quietly into rambling talk of his last three
years, and, without ever telling his story right out, left a strange
picturesque impression of a nomadic life which struck one as separated
by fifty years from modern conventional existence. The old road-life
still lingered on in places, it seemed, once one got well away from
the railway: there were two Englands existing together, the one
fringing the great iron highways wherever they might go~-- the England
under the eyes of most of us. The other, unguessed at by many, in
whatever places were still vacant of shriek and rattle, drowsed on as
of old: the England of heath and common and windy sheep down, of
by-lanes and village-greens~-- the England of Parson Adams and
Lavengro. The spell of the free untrammelled life came over me as I
listened, till I was fain to accept of his hospitality and a
horse-blanket for the night, oblivious of civilised comforts down at
the Bull. On the downs where Alfred fought we lay and smoked, gazing
up at the quiet stars that had shone on many a Dane lying stark and
still a thousand years ago; and in the silence of the lone tract that
enfolded us we seemed nearer to those old times than to these I had
left that afternoon, in the now hushed and sleeping valley of the
Thames.

When the news reached me, some time later, that Fother\-gill's aunt had
died and left him her house near town and the little all she had
possessed, I heard it with misgivings, not to say forebodings. For
the house had been his grandfather's, and he had spent much of his
boyhood there; it had been a dream of his early days to possess it in
some happy future, and I knew he could never bear to sell or let
it. On the other hand, can you stall the wild ass of the desert? And
will not the caged eagle mope and pine?

However, possession was entered into, and all seemed to go well for
the time. The cart was honourably installed in the coach-house, the
mare turned out to grass. Fothergill lived idly and happily, to all
seeming, with \quote{a book of verses underneath the bough,} and a
bottle of old claret for the friend who might chance to drop in. But
as the year wore on small signs began to appear that he who had always
\quote{rather hear the lark sing than the mouse squeak} was beginning
to feel himself caged, though his bars were gilded.

I was talking one day to his coachman (he now kept three
men-servants), and he told me that of a Sunday morning when the
household had gone to church and everything was quiet, Mr~Fothergill
would go into the coach-house and light his pipe, and sit on the step
of the brougham (he had a brougham now), and gaze at the old cart, and
smoke and say nothing; and smoke and say nothing again. He didn't like
it, the coachman confessed; and to me it seemed ominous.

One morning late in March, at the end of a long hard winter, I was
wakened by a flood of sunshine. The early air came warm and soft
through the open window; the first magic suggestion of spring was
abroad, with its whispered hints of daffodils and budding hawthorns;
and one's blood danced to imagined pipings of Pan from happy fields
far distant. At once I thought of Fothergill, and, with a certain
foreboding of ill, made my way down to Holly Lodge as soon as
possible. It was with no surprise at all that I heard that the master
was missing. In the very first of the morning, it seemed, or ever the
earliest under-housemaid had begun to set man-traps on the stairs and
along the passages, he must have quietly left the house. The servants
were cheerful enough, nevertheless, and thought the master must only
have \quote{gone for a nice long walk,} and so on, after the manner of
their kind. Without a word I turned my steps to the coach-house. Sure
enough, the old cart was missing; the mare was gone from the
paddock. It was no good my saying anything; pursuit of this wild
haunter of tracks and by-paths would have been futile indeed. So I
kept my own counsel. Fothergill never returned to Holly Lodge, and has
been more secret and evasive since his last flight, rarely venturing
on old camping grounds near home, like to a bird scared by the
fowler's gun.

Once indeed, since then, while engaged in pursuit of the shy quarry
known as the Early Perp., late Dec., E. Eng., and the like, specimens
of which I was tracking down in the west, I hit upon him by accident;
hearing in an old village rumours concerning a strange man in a cart
who neither carried samples nor pushed the brewing interest by other
means than average personal consumption~-- tales already beginning to
be distorted into material for the myth of the future. I found him
friendly as ever, equally ready to spin his yarns. As the evening wore
on, I ventured upon an allusion to past times and Holly Lodge; but his
air of puzzled politeness convinced me that the whole thing had passed
out of his mind, as a slight but disagreeable incident in the even
tenor of his nomadic existence.

After all, his gains may have outbalanced his losses. Had he cared, he
might, with his conversational gifts, have been a social success;
certainly, I think, an artistic one. He had great powers, had any
impulse been present to urge him to execution and achievement. But he
was for none of these things. Contemplative, receptive, with a keen
sense of certain sub-tones and side aspects of life unseen by most, he
doubtless chose wisely to enjoy life his own way, and to gather from
the fleeting days what bliss they had to give, nor spend them in
toiling for a harvest to be reaped when he was dust.

\startnarrower
\startlines
Some for the glories of this life, and some
Sigh for the Prophet's Paradise to come:
Ah, take the cash and let the credit go,
Nor heed the rumble of a distant drum.
\stoplines
\stopnarrower
