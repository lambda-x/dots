;; ;;;; -*- lexical-binding: t -*-
;; (defvar file-name-handler-alist-old file-name-handler-alist)

;;   ;; (setq package-enable-at-startup nil
;;   ;; file-name-handler-alist nil
;;   ;; message-log-max 16384
;;   ;; gc-cons-threshold (* 1000 1000 1000)
;;   ;; gc-cons-percentage 0.6
;;   ;; auto-window-vscroll nil)
;;   ;; The default is 800 kilobytes.  Measured in bytes.

;; (setq gc-cons-threshold (* 100 1024 1024))

;; ;;   (defvar file-name-handler-alist-backup file-name-handler-alist)
;; ;; (setq gc-cons-threshold most-positive-fixnum)

;;   (add-hook 
;;    'after-init-hook
;;       `(lambda ()
;;          (setq file-name-handler-alist file-name-handler-alist-old
;;          gc-cons-threshold 2000000
;;          gc-cons-percentage 0.1)
;;          (garbage-collect)) t)

;; ;; (add-hook 'after-init-hook
;; ;;   (lambda ()
;; ;;     (garbage-collect)
;; ;;     (setq gc-cons-threshold
;; ;;             (car (get 'gc-cons-threshold 'standard-value))
;; ;;       file-name-handler-alist
;; ;;         (append
;; ;;           file-name-handler-alist-backup
;; ;;           file-name-handler-alist))))

;; (defun my-minibuffer-setup-hook ()
;;   (setq gc-cons-threshold most-positive-fixnum))

;; (defun my-minibuffer-exit-hook ()
;;   (setq gc-cons-threshold 800000))

;; (add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
;; (add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)
;; (setq read-process-output-max (* 1024 1024)) ;; 1mb

;;   ;; Use this to show what packages are being loaded when. Very useful
;;   ;; when trying to optimize startup time.
;;   ;; (setq use-package-verbose t)

