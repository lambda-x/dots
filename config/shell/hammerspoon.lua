hyper = { "ctrl", "cmd", "alt" }

hs.hotkey.bind(hyper, "h",
  function()
    hs.alert.show("Hello World!")
    end
)

hs.hotkey.bindSpec({ hyper, "r" }, hs.reload)
hs.hotkey.bind(hyper, "e",
  function()
  os.execute("emacsclient -c 'org-protocol://capture?template=b'")
  end
)

hs.hotkey.bind("cmd", 'H', function()end)

hs.notify.show("hammer is started tho", "", "")
