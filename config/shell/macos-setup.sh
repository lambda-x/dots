#!/usr/bin/env bash
# Setup script for setting up a new macOS machine

echo "Starting setup"

# install xcode CLI
xcode-select --install

git clone git@gitlab.com:lambda-x/dots.git

# Check for Homebrew to be present, install if it's missing
if test ! $(which brew); then
    echo "Installing homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Update Homebrew
brew update

# Tap for Mac Port of Emacs
brew tap railwaycat/emacsmacport

# Tap for Cask Fonts
brew tap homebrew/cask-fonts

PACKAGES=(
    aspell
    ffmpeg
    fzf
    git
    python3
    ripgrep
    youtube-dl
    zsh
)

# # Homebrew formulae
brew install emacs-mac --with-emacs-big-sur-icon
brew install luarocks --HEAD
brew install neovim --HEAD

echo "Installing packages..."
brew install ${PACKAGES[@]}

# Install FZF keybindings for the shell.
$(brew --prefix)/opt/fzf/install

# Link my Emacs config files.
mkdir -p ~/.config/emacs
ln -s ~/dots/emacs/straight-config.el ~/.config/emacs/init.el
ln -s ~/dots/emacs/early-init.el ~/.config/emacs/early-init.el

# Link my Neovim config.
mkdir -p ~/.config/nvim
ln -s ~/dots/neovim/init.vim ~/.config/nvim/init.vim

# Install Brew Casks
echo "Installing cask..."

CASKS=(
    alfred
    bitwarden
    djview
    dropbox
    firefox
    font-et-book
    font-fantasque-sans-mono-nerd-font
    font-hack-nerd-font
    font-ibm-plex
    font-source-sans-pro
    font-vollkorn
    iterm2
    karabiner-elements
    mactex-no-gui
    microsoft-edge
    mpv
    spotify
    the-unarchiver
    visual-studio-code
    
)

echo "Installing cask apps..."
brew install --cask ${CASKS[@]}

# Brew installs binaries to sbin, so put it on the path.
echo 'export PATH="/usr/local/sbin:$PATH"' >> ~/.zshrc

# Write some defaults
echo "Configuring OS..."

# Set fast key repeat rate
# defaults write NSGlobalDomain KeyRepeat -int 0

# Use a transparent titlebar for Emacs
defaults write org.gnu.Emacs TransparentTitleBar LIGHT

# Install Zinit
sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"

# Add some customization to .zshrc
echo "
zinit light zdharma/fast-syntax-highlighting

zinit ice complie'(pure|async)' pick"async.zsh" src"pure.zsh"
zinit light sindresorhus/pure

PURE_PROMPT_SYMBOL=λ
" >> ~/.zshrc

# Silence the "last login..." prompt
touch .hushlogin

echo "Macbook setup completed!"
