nnoremap gr <Cmd>call VSCodeNotify('editor.action.goToReferences')<CR>
nnoremap K <Cmd>call VSCodeNotify('editor.action.showHover')<CR>
nnoremap gd <Cmd>call VSCodeNotify('editor.action.revealDefinition')<CR>
nnoremap gd <Cmd>call VSCodeNotify('editor.action.revealDefinition')<CR>
nnoremap g. <Cmd>call VSCodeNotify('editor.action.quickFix')<CR>
