local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end

vim.opt_global.shortmess:remove("F")

require 'impatient'
require "plugins"
require "options"
require "bind"
require "settings"

-- vim.cmd([[
--   augroup packer_user_config
--     autocmd!
--     autocmd BufWritePost plugins.lua source <afile> | PackerCompile
--   augroup end
-- ]])

-- This is a great guide to writing a config in Lua.
-- It especially helped me understand all the stuffabout
-- about why people are returning
-- tables in their configs.
-- Found while searching "neovim return table m"
-- https://www.gatlin.io/blog/post/lua-primer-for-neovim
