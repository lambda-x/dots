local api = vim.api
local cmd = vim.cmd
local u = require('utils')
local f = u.make_keymap
local m = u.keymaps
   
local setup = function() 


  ----------------------------------
  -- LSP Setup ---------------------
  ----------------------------------
  local metals_config = require("metals").bare_config()

  -- Example of settings
  metals_config.settings = {
    showImplicitArguments = true,
    excludedPackages = { "akka.actor.typed.javadsl", "com.github.swagger.akka.javadsl" },
    serverVersion = "latest.snapshot"
  }

  local function map(mode, lhs, rhs, opts)
    local options = { noremap = true }
    if opts then
      options = vim.tbl_extend("force", options, opts)
    end
    api.nvim_set_keymap(mode, lhs, rhs, options)
  end

  -- local buf = "vim.lsp.buf."

  -- local lsp_leader_map = {
  --   mc = "'telescope'.extensions.metals.commands()",
  --   cl = "vim.lsp.codelens.run()",
  --   sh = buf.."signature_help()",
  --   rn = buf.."rename()",
  --   fd = buf.."formatting()",
  --   ca = buf.."code_action()",
  --   ws = "'metals'.hover_worksheet()",
  -- }

  -- for k,v in pairs(lsp_leader_map) do
  --   f(k,v,m.LEADER)
  -- end

 
  -- *READ THIS*
  -- I *highly* recommend setting statusBarProvider to true, however if you do,
  -- you *have* to have a setting to display this in your statusline or else
  -- you'll not see any messages from metals. There is more info in the help
  -- docs about this
  metals_config.init_options.statusBarProvider = "on"

  -- Example if you are using cmp how to make sure the correct capabilities for snippets are set
  local capabilities = vim.lsp.protocol.make_client_capabilities()
  capabilities.textDocument.completion.completionItem.snippetSupport = true
  metals_config.capabilities = capabilities

  -- Debug settings if you're using nvim-dap
  local dap = require("dap")

  dap.configurations.scala = {
    {
      type = "scala",
      request = "launch",
      name = "RunOrTest",
      metals = {
        runType = "runOrTestFile",
        --args = { "firstArg", "secondArg", "thirdArg" }, -- here just as an example
      },
    },
    {
      type = "scala",
      request = "launch",
      name = "Test Target",
      metals = {
        runType = "testTarget",
      },
    },
  }
  
  metals_config.on_attach = function(client, bufnr)
    require("lsp.init").on_attach(client, bufnr)
  -- LSP mappings

  map("n", "<leader>mc", [[<cmd>lua require("telescope").extensions.metals.commands()<CR>]])


  -- Example mappings for usage with nvim-dap. If you don't use that, you can
  -- skip these
  map("n", "<leader>dc", [[<cmd>lua require"dap".continue()<CR>]])
  map("n", "<leader>dr", [[<cmd>lua require"dap".repl.toggle()<CR>]])
  map("n", "<leader>dK", [[<cmd>lua require"dap.ui.widgets".hover()<CR>]])
  map("n", "<leader>dt", [[<cmd>lua require"dap".toggle_breakpoint()<CR>]])
  map("n", "<leader>dso", [[<cmd>lua require"dap".step_over()<CR>]])
  map("n", "<leader>dsi", [[<cmd>lua require"dap".step_into()<CR>]])
  map("n", "<leader>dl", [[<cmd>lua require"dap".run_last()<CR>]])


  -- on_attach(client, buffer)
    require("metals").setup_dap()
    map("n", "<leader>mc", [[<cmd>lua require("telescope").extensions.metals.commands()<CR>]])
  end

  -- Autocmd that will actually be in charging of starting the whole thing
  local nvim_metals_group = api.nvim_create_augroup("nvim-metals", { clear = true })
  api.nvim_create_autocmd("FileType", {
    -- NOTE: You may or may not want java included here. You will need it if you
    -- want basic Java support but it may also conflict if you are using
    -- something like nvim-jdtls which also works on a java filetype autocmd.
    pattern = { "scala", "sbt", "java" },
    callback = function()
      require("metals").initialize_or_attach(metals_config)
    end,
    group = nvim_metals_group,
  })

end

return {
    setup = setup
  }
