-- local nvim_lsp = require 'lspconfig'
-- local ts_utils = require("nvim-lsp-ts-utils")

-- local ts_utils_settings = {
--   enable_formatting = true,
--   formatter = "prettier",
-- }

-- local M = {}
-- M.setup = function(on_attach)
--   nvim_lsp.tsserver.setup({
--     on_attach = function(client, bufnr)
--       -- disable tsserver formatting if you plan on formatting via null-ls
--       client.resolved_capabilities.document_formatting = false
--       client.resolved_capabilities.document_range_formatting = false


--       on_attach(client, bufnr)

--       -- defaults
--       ts_utils.setup(ts_utils_settings) 

--       -- required to fix code action ranges
--       ts_utils.setup_client(client)


--       -- define an alias
--       vim.cmd("command -buffer Formatting lua vim.lsp.buf.formatting()")
--       vim.cmd("command -buffer FormattingSync lua vim.lsp.buf.formatting_sync()")

--       -- format on save
--       vim.cmd("autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()")


--       -- no default maps, so you may want to define some here
--       local opts = { silent = true }
--       vim.api.nvim_buf_set_keymap(bufnr, "n", "gs", ":TSLspOrganize<CR>", opts)
--       vim.api.nvim_buf_set_keymap(bufnr, "n", "qq", ":TSLspFixCurrent<CR>", opts)
--       vim.api.nvim_buf_set_keymap(bufnr, "n", "gr", ":TSLspRenameFile<CR>", opts)
--       vim.api.nvim_buf_set_keymap(bufnr, "n", "gi", ":TSLspImportAll<CR>", opts)
--     end
--   })
-- end

-- return M
