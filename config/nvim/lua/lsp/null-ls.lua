local null = require("null-ls")
local b = null.builtins

local sources = {
  b.formatting.prettierd.with({
    filetypes = { "html", "json", "yaml", "markdown"},
  })
}

local M = {}
M.setup = function(on_attach)
  null.config({
    sources = sources,
  })
  require("lspconfig")["null-ls"].setup { 
    on_attach = on_attach,
  }
end

return M
