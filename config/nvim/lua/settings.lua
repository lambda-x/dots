-- The colorscheme still requires Vim syntax to set.
vim.o.termguicolors = true
-- vim.o.background = "dark"
-- vim.g.everforest_background = "soft"
-- vim.g.everforest_enable_italic = 1
-- vim.g.everforest_diagnostic_text_highlight = 1
-- vim.g.everforest_diagnostic_virtual_text = "colored"
-- vim.g.everforest_current_word = "bold"
-- vim.g.everforest_ui_contrast = "high"
-- vim.g.gruvbox_material_background = "soft"
-- vim.g.gruvbox_material_palette = "original"
-- vim.g.gruvbox_material_better_performance = 1
 -- vim.cmd([[colorscheme github-light ]])
--  require('github-theme').setup({
--   theme_style = "dark",
--   function_style = "bold",
-- })
 -- vim.g.nightfox_style = "nordfox"
 -- require('nightfox').set()
-- vim.g.material_style = "lighter"
-- vim.g.aquarium_style = "light"

require('onedark').setup({
  comment_style = "NONE",
  keyword_style = "NONE",
  function_style = "NONE",
  variable_style = "NONE"
})

-- vim.cmd 'colorscheme nordfox'
-- vim.g.rooter_change_directory_for_non_project_files = "current"
-- vim.g.neovide_cursor_animation_length=0.05

-- colorscheme gruvbox-material

local default_options = {
  backup = false,
  clipboard = unnamedplus,
  cmdheight = 2,
  completeopt = { "menuone", "noselect" },
  fileencoding = "utf-8",
  foldenable = false,
  hidden = true,
  hlsearch = false,
  ignorecase = true,
  mouse = "a",
  pumheight = 10,
  smartcase = true,
  smartindent = true,
  splitbelow = true,
  splitright = true,
  swapfile = false,
  expandtab = true,
  shiftwidth = 2,
  tabstop = 2,
  cursorline = false, -- Highlight the entire line
  number = true,
  relativenumber = true,
  signcolumn = "yes",
  scrolloff = 8,
}

for k, v in pairs(default_options) do
  vim.opt[k] = v
end
