-- This table is used as an enum.
-- See: https://unendli.ch/posts/2016-07-22-enumerations-in-lua.html
local keymaps = {
  LEADER = 1,
  NORMAL = 2,
  VISUAL = 3,
}

-- The '..' operator concats strings
local function make_keymap(k, c, e)
  local o = {noremap = true, silent = true}
  local l = '<cmd>lua require'

  if string.find(c, ":") then
    l = ''
  end

  if (e == keymaps.LEADER) then
    vim.api.nvim_set_keymap('n', '<Leader>'..k, l..c..'<CR>', o)
  end

  if (e == keymaps.NORMAL) then
    vim.api.nvim_set_keymap('n', k, l..c..'<CR>', o)
  end 

end

return {
  make_keymap = make_keymap, 
  keymaps = keymaps,
}
