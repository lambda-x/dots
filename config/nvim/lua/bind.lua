local g = vim.g
local u = require('utils')
local f = u.make_keymap
local m = u.keymaps

g.mapleader = ','
g.maplocalleader = ','

local leader_map = {
  b  = "'telescope.builtin'.buffers()",
  e  = "'telescope.builtin'.commands()",
  h  = "'fzf-lua'.help_tags()",
  s  = ":source %",
  ff = "'telescope.builtin'.find_files()",
  fs = ":w",
  gf = "'telescope.builtin'.git_files()",
  gs = ":Neogit",
  pp = "'telescope'.extensions.project.project{}",
}

local normal_map = {
  s = "'hop'.hint_char2()"
}

for k, v in pairs(leader_map) do
  f(k,v,m.LEADER)
end

for k,v in pairs(normal_map) do
  f(k,v,m.NORMAL)
end
