local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end

vim.cmd('packadd packer.nvim')

return require('packer').startup(
  function ()

    use {
      'tpope/vim-surround',
      event = 'BufEnter'
    }

    use {
      'tpope/vim-commentary',
      event = 'BufEnter'
    }

    use {
      'phaazon/hop.nvim',
      config = function()
      require 'hop'.setup({
          vim.api.nvim_set_keymap('n', 's', "<cmd>lua require'hop'.hint_char2()<cr>", {noremap = true, silent = true })
        }) end
    }
    -- use {
    --   'ggandor/lightspeed.nvim',
    --   config = function()
    --     require "plugins.lightspeed"
    --   end
    -- }

end
)

