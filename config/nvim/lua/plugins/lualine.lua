local function metals_stuff()
  return vim.g['metals_status'] or ""
end

local function hello()
  return [[hello world!!!]]
end

local function setup()
  require('lualine').setup {
    options = { 
      theme = 'auto' 
    },
    sections = {
      lualine_c = { 'filename', metals_stuff }
    }
  }
end

return {
  setup = setup
}
