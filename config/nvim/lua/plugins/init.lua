vim.cmd('packadd packer.nvim')

return require('packer').startup(
  function ()

    use 'wbthomason/packer.nvim'

    use {
      'lewis6991/impatient.nvim',
    }

    use {
      'nvim-lualine/lualine.nvim',
      requires = { 'kyazdani42/nvim-web-devicons', opt = true },
      config = function()
        require('plugins.lualine').setup()
      end
    }

    use {
      'junegunn/fzf'
    }

    use {
      'kristijanhusak/orgmode.nvim',
      config = function()
        require'orgmode'.setup()
        require'orgmode'.setup_ts_grammar()
      end
    }

    use {
      'ibhagwan/fzf-lua',
      requires = {
        'vijaymarupudi/nvim-fzf',
        'kyazdani42/nvim-web-devicons' },
    }

    use {
       'tpope/vim-surround',
       event = 'BufRead'
    }
    
    use {
       'tpope/vim-commentary',
       event = 'BufEnter'
    }
    
    use {
      'dstein64/vim-startuptime',
      cmd = 'StartupTime'
    }

    -- LSP Plugins
    use {
      'neovim/nvim-lspconfig',
      event = 'BufRead',
      requires = {
                'jose-elias-alvarez/nvim-lsp-ts-utils',
                module = 'nvim-lsp-ts-utils'
            },
      config = function()
        require "lsp"
      end
    }
    
    use {
      'jose-elias-alvarez/null-ls.nvim',
      config = function ()
        require "lsp"
      end,
      after = 'nvim-lspconfig'

    }

    -- use {
    --   'ray-x/lsp_signature.nvim',
    --   after = "nvim-lspconfig",
    --   config = function()
    --     require("lsp_signature").setup()
    --   end
    -- }

    use {
      'tami5/lspsaga.nvim',
      after = 'nvim-lspconfig',
    }
 
    -- Completion
    use {
      'hrsh7th/nvim-cmp',
      -- event = 'BufEnter',
      requires = {
        'onsails/lspkind-nvim',
        'hrsh7th/vim-vsnip',
        'hrsh7th/cmp-vsnip',
        -- 'hrsh7th/vim-vsnip-integ',
        'hrsh7th/cmp-buffer',
        'hrsh7th/cmp-cmdline',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-nvim-lua',
        'hrsh7th/cmp-nvim-lsp-document-symbol',
        'hrsh7th/cmp-nvim-lsp-signature-help',
      },
      config = function()
        require "plugins.cmp"
      end,
    }

    -- Etc.
    -- use 'airblade/vim-rooter'

    -- Treesitter
    use {
      "nvim-treesitter/nvim-treesitter",
      -- Only use the following for stable Nvim
      -- branch = '0.5-compat',
      run = ':TSUpdate',
      config = function ()
        require "plugins.treesitter"
      end
    }

    use { 'nvim-treesitter/nvim-treesitter-textobjects',
      -- Stable Nvim only
      -- branch = '0.5-compat',
      after = "nvim-treesitter",
    }    

    use {
      'nvim-treesitter/playground',
      cmd = "TSPlaygroundToggle"
    }

    -- Telescope

    use {
      'nvim-telescope/telescope.nvim',
      -- cmd = "Telescope",
      event = 'BufRead',
      module = 'telescope',
      requires = {
        'nvim-lua/popup.nvim',
        'nvim-lua/plenary.nvim'
      },
      config = function ()
        require("plugins.telescope")
      end
    }

    use {
      'nvim-telescope/telescope-project.nvim',
      -- after = 'telescope.nvim'
    }

    -- use {
    --   "nvim-telescope/telescope-frecency.nvim",
    --   config = function()
    --     require"telescope".load_extension("frecency")
    --   end,
    --   requires = {"tami5/sql.nvim"}
    -- }

    -- use {
    --   'nvim-telescope/telescope-fzf-native.nvim',
    --   run = 'make',
    -- }

    use {
      'nvim-telescope/telescope-fzy-native.nvim'
    }

    -- Color Themes
    use 'ful1e5/onedark.nvim'
    use 'rebelot/kanagawa.nvim'
    use 'EdenEast/nightfox.nvim'
    use {
      'projekt0n/github-nvim-theme',
    }
    -- use 'rmehri01/onenord.nvim'
    -- use 'ellisonleao/gruvbox.nvim'
    -- use 'frenzyexists/aquarium-vim'
    -- use 'sainnhe/everforest'
    -- use({ 'rose-pine/neovim', as = 'rose-pine' })
    -- use 'YorickPeterse/vim-paper'
    -- use 'mcchrish/zenbones.nvim'

    -- use 'marko-cerovac/material.nvim'
    -- use 'theniceboy/nvim-deus'
    -- use 'sainnhe/gruvbox-material'
    -- use 'savq/melange'
    -- use({
    --   "catppuccin/nvim",
    --   as = "catppuccin"
    -- })
    -- use 'shaunsingh/nord.nvim'
    -- use 'mhartington/oceanic-next'

    -- --                      -- --
    -- -- Navigation & Editing -- --
    -- --                      -- --

    -- use {
    --   'ggandor/lightspeed.nvim',
    --   config = function()
    --     require "plugins.lightspeed"
    --   end
    -- }

    use {
      'phaazon/hop.nvim',
      branch = 'v1',
      event = 'BufEnter',
      config = function()
        require 'hop'.setup({
          -- vim.api.nvim_set_keymap.map(bufnr, 'n', 's', "<cmd>lua require'hop'.hint_char2()<cr>", {noremap = true, silent = true })
        })
      end
    }

    use {
      'windwp/nvim-autopairs',
      after = "nvim-cmp",
      -- event = "ColorScheme",
      config = function()
        require("plugins.autopairs")
      end
    }
      
    -- use 'wellle/targets.vim'

    use { 
      'LnL7/vim-nix',
      event = BufEnter,
    }

    use {
      'windwp/nvim-ts-autotag',
      -- after = "nvim-treesitter"
    }

    -- use {
    --   "ahmedkhalf/project.nvim",
    --   config = function()
    --     require("project_nvim").setup()
    --   end
    -- }

    use 'direnv/direnv.vim'

    use {
      'TimUntersberger/neogit',
      requires = 'nvim-lua/plenary.nvim',
      cmd = 'Neogit',
      -- cmd = "<cmd>lua require'neogit'.open()<cr>",
      -- cmd = "neogit.open()",
      config = function()
        require'neogit'.setup {
          disable_commit_confirmation = true,
        }
      end,
    }

    use {
      'lukas-reineke/indent-blankline.nvim',
      cmd = 'IndentBlanklineEnable'
    }

    -- use {
    --   'ms-jpq/coq_nvim',
    --   branch = 'coq',
    --   event = "InsertEnter",
    --   cmd = "COQnow"
    -- }

    use {
      "scalameta/nvim-metals",
      requires = {
        "nvim-lua/plenary.nvim",
        "mfussenegger/nvim-dap",
      },
      config = function()
        require("lsp.metals").setup()
      end
    }

  end
)

-- Good configs that utilize lazy loading:
-- https://github.com/budswa/nvim
-- vhyrro/neovhy
-- https://github.com/shaunsingh/vimrc-dotfiles
-- https://github.com/FotiadisM/nvim-lua-setup
-- https://www.reddit.com/r/neovim/comments/opipij/guide_tips_and_tricks_to_reduce_startup_and/
-- Check in on Neorg later
-- Check out https://github.com/simrat39/symbols-outline.nvim
-- https://github.com/jsit/toast.vim


-- To use latest version, replace current grammar installation with "require('orgmode').setup_ts_grammar()" and
-- run :TSUpdate org
