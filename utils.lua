-- The '..' operator concats strings

local function make_map(k, c)
  local o = {noremap = true, silent = true}
  local l = '<cmd>lua require'

  if string.find(c, ":") then
    l = ''
  end

  vim.api.nvim_set_keymap('n', '<Leader>'..k, l..c..'<CR>', o)
end

local function nizzle(k, c)
  local o = {noremap = true, silent = true}
  local l = '<cmd>lua require'

  if string.find(c, ":") then
    l = ''
  end

  vim.api.nvim_set_keymap('n', k, l..c..'<CR>', o)
end

return {
  make_map = make_map,
  neezy = nizzle,
}
