{ config, pkgs, ... }:

{
  imports = [
    ./home-common.nix
    ./keyboard
  ];

  home.username = "anthony";
  home.homeDirectory = "/home/anthony";

  home.packages = with pkgs; [
    autorandr
    # binutils                    # For EmacsGcc
    bitwarden
    brightnessctl
    chromium
    evince
    feh
    # firefox
    # google-chrome
    kdeApplications.okular
    python3 # For Treemacs

    # .Net
    dotnet-sdk_5
    mono6
    omnisharp-roslyn

    # Multimedia
    spotify
    
    xcape
    xorg.xev
    xorg.xkbcomp
    xorg.xmodmap

    dmenu # for Xmonad
    polybar
    stalonetray
    
    # haskellPackages.xmonad-contrib
    # haskellPackages.xmobar
    # haskellPackages.monad-logger
    
  ];
  
  programs = {
    chromium = {
      enable = true;
      extensions = [
        "nngceckbapebfimnlniiiahkandclblb" # Bitwarden
        "hhhpdkekipnbloiiiiaokibebpdpakdp" # Saka-Key
        "pgjjikdiikihdfpoppgaidccahalehjh" # Speedtest
        "cjpalhdlnbpafiamejdnhcphjbkeiagm" # Ublock-Origin
      ];
    };
    rofi = {
      enable = true;
      font = "hack 20";
      borderWidth = 5;
      theme = "Arc-Dark";
    };
    zsh = {
      enable = true;
    };
  };
  
  services = {
    dropbox = {
      enable = true;
    };
    lorri = {
      enable = true;
    };
  };
  
  # xsession = {
  #   enable = true;
  #   initExtra = ''
  #    xmodmap ~/.Xmodmap
  #    autorandr --change
  #    # dropbox start &
  #    # feh --bg-fill ~/Downloads/john-fowler-RsRTIofe0HE-unsplash.jpg &
  #    # stalonetray &
  #    '';
  #  #  windowManager = {
  # #     xmonad = {
  # #       enable = true;
  # #       enableContribAndExtras = true;
  # #     };
  # #   };
  #  };

}
