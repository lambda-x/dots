{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    alacritty    
    aspell
    aspellDicts.en
#    cachix
    direnv
    guile
    ffmpeg
    # hugo
    
    pandoc
    manix
    mu

    fzy

    nano
    # texlive.combined.scheme-full
    # libgccjit    
    mpv
    coreutils
    curl
    nix-prefetch-git
    poppler
    python3
    ripgrep
    tesseract
    wordnet
    xclip
    youtube-dl
    yt-dlp
  ];

  # programs.bash.enable = true;
  programs.gpg.enable = true;
  
}
