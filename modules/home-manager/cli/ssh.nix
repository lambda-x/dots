{ config, pkgs, ... }:
{
  programs.ssh = {
    enable = true;
    extraConfig = ''
    Host office
         HostName 67.48.51.166
         User root
         Port 1240

    Host porcinity
         HostName 209.145.52.213
         User porcinity
         Port 2929
    '';
  };
}
