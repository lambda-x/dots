{ pkgs, config, ... }:

{
  imports = [
    ./cli.nix
    ./direnv.nix
    ./fzf.nix
    ./git.nix
    ./ssh.nix
    ./tmux.nix
    ./zsh.nix
  ];
}
