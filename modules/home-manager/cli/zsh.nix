{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    pure-prompt
  ];

  programs = {
    zsh = {
      enable = true;
      autocd = true;
      cdpath = [
        "~/source"
        "~/source/scala/"
        "~/source/dotnet"
        "~/dots/config/nvim"
        "~/dots/modules"
      ];
      dotDir = ".config/zsh";
      dirHashes = {
        dn = "$HOME/source/dotnet";
      };
      enableCompletion = true;
      enableAutosuggestions = false;
      defaultKeymap = "emacs";
      sessionVariables = {
        PURE_PROMPT_SYMBOL="𝛌";
        LSP_USE_PLISTS=true;
      };
      envExtra = ''
        # 10ms delay for esc.
        KEYTIMEOUT=1

        export FZF_COMPLETION_TRIGGER=','

        # Delete and Select words in Emacs mode.
        bindkey "^[[3;3~" delete-word
        bindkey "^[[1;4C" select-a-word

        # Add Metals bin to path
        path+=('/Users/anthony/Library/Application Support/Coursier/bin')
      '';

      initExtra = ''
        # On macOS, alt+c enters the Latin script letter ç. This allows
        # us to use alt+c as suggested on the fzf repo.

        bindkey "ç" fzf-cd-widget

        vterm_printf(){
        if [ -n "$TMUX" ] && ([ "''${TERM%%-*}" = "tmux" ] || [ "''${TERM%%-*}" = "screen" ] ); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1" 
        elif [ "''${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
        else
        printf "\e]%s\e\\" "$1"
        fi
        }
      '';

      history = {
        size = 50000;
        save = 500000;
        ignoreDups = true;
        share = true;
        ignoreSpace = true;
      };

      plugins = [
        {
          name = "pure";
          src = pkgs.fetchFromGitHub {
            owner = "sindresorhus";
            repo = "pure";
            rev = "5b458ba5b75f49a8071d53c343f1a23631f7bced";
            sha256 = "1bxg5i3a0dm5ifj67ari684p89bcr1kjjh6d5gm46yxyiz9f5qla";
          };
        }
      ];

      shellAliases = {
        v = "nvim";
        e = "emacsclient -c -n -a ''";
        ek = "emacsclient -e \"(kill-emacs)\"";
      };

    };
  };
}
