{ config, pkgs, ... }:
{
  programs = {

    tmux = {
      enable = true;
      extraConfig = ''
        source dots/shell/tmux.conf
    '';
    };

  };
}
