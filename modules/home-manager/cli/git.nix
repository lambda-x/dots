{ config, pkgs, ... }:
{
  programs = {
    git = {
      enable = true;

      userEmail = "it@me.org";

      userName = "Ant";

      ignores = [
        "*.DS_Store"
        ".bloop/"
        ".bsp/"
        ".metals/"
        "*.swp"
        "/Library/**"
        "~/Library/"
        "/Library/Application\ Support/"
        "node_modules/"
        ".direnv/"
        ".envrc"
        ".log/"
      ];
      
    };
  };
}
