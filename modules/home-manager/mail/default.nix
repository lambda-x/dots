{ config, pkgs, ... }:
{
  imports = [ ./mbsync.nix
              ./gmail.nix
              ./outlook.nix
            ];
}
