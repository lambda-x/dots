{ pkgs, config, ... }:
{
  programs = {
    mbsync = {
      extraConfig = ''
        IMAPAccount outlook
        Host imap.outlook.com
        User anthony.adrian@outlook.com
        PassCmd "gpg --quiet --for-your-eyes-only --no-tty --decrypt ~/outlook.gpg"
        SSLType IMAPS
        CertificateFile /etc/ssl/certs/ca-certificates.crt

        IMAPStore outlook-remote
        Account outlook

        MaildirStore outlook-local
        Subfolders Verbatim
        Path ~/Mail/outlook/
        Inbox ~/Mail/outlook/Inbox

        Channel outlook
        Far :outlook-remote:
        Near :outlook-local:
        Create Both
        Expunge Both
        Patterns *
        SyncState *
    '';
    };
  };
}
