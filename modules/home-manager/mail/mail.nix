{ config, ...}:

let
  gmail = "anthony.c.adrian@gmail.com";
  outlook = "anthony.adrian@outlook.com";
  homedir = /Users/anthony;
in

{
  
  accounts = {

    email = {

      maildirBasePath = "${homedir}/Mail";

      accounts = {

        Outlook = {
          primary = true;
          realName = "Anthony Adrian";
          address = "${outlook}";
          folders = {
            inbox = "Inbox";
          };

          imap = {
            host = "imap.outlook.com";
            # port = 993;
          };

          gpg.key = "0x181C2BD7178F92E8";
          gpg.signByDefault = true;

          smtp = {
            host = "smtp.outlook.com";
            # port = 587;
          };

          userName = "${outlook}";

          mbsync = {
            enable = true;
          };

          # offlineimap.enable = true;
          
          passwordCommand = "cat ~/outlook";

        };
      };
    };
  };

  # programs.mbsync = {
  #   enable = true;
  #   # create = "both";
  #   # expunge = "none";
  #   # patterns = [ "*" "[Gmail]*" ];
  # };

  # programs.offlineimap.enable = true;

  # programs.msmtp.enable = true;


}
