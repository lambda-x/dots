{ config, pkgs, ... }:
{
  programs = {
    mbsync = {
      extraConfig = ''
        IMAPStore gmail-remote
        Host imap.gmail.com
        SSLType IMAPS
        AuthMechs LOGIN
        User anthony.c.adrian@gmail.com
        PassCmd "gpg --quiet --for-your-eyes-only --no-tty --decrypt ~/gmail.gpg"

        MaildirStore gmail-local
        Path ~/Mail/gmail/
        Inbox ~/Mail/gmail/Inbox
        Subfolders Verbatim

        Channel sync-googlemail-default
        Far :gmail-remote:
        Near :gmail-local:
        # Select some mailboxes to sync
        Patterns "INBOX"

        Channel sync-googlemail-trash
        Far :gmail-remote:"[Gmail]/Trash"
        Near :gmail-local:Trash
        Create Near

        Channel sync-googlemail-sent
        Far :gmail-remote:"[Gmail]/Sent Mail"
        Near :gmail-local:Sent
        Create Near

        Channel sync-googlemail-all
        Far :gmail-remote:"[Gmail]/All Mail"
        Near :gmail-local:"All Mail"
        Create Both
        Expunge Both

        Channel sync-googlemail-drafts
        Far :gmail-remote:"[Gmail]/Drafts"
        Near :gmail-local:Drafts
        Create Near

        Group googlemail
        Channel sync-googlemail-default
        Channel sync-googlemail-sent
        Channel sync-googlemail-all
        Channel sync-googlemail-trash


        # Channel gmail
        # Far :gmail-remote:
        # Near :gmail-local:
        # Create Both
        # Expunge Both
        # # Patterns * !"[Gmail]/All Mail" !"[Gmail]/Sent Mail" !"[Gmail]/Starred" !"[Gmail]/Trash"
        # # SyncState *
    '';
    };
  };
}
