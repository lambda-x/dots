{ config, pkgs, lib, ... }:

{
  imports = [ ./home.nix ];

  # home.username = "anthony";
  # home.homeDirectory = "/Users/anthony";

  home.packages = with pkgs; [
    pinentry_mac
  ];
}
