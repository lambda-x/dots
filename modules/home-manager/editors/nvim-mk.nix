{ config, options, lib, pkgs, ... }:

with lib;
let
  cfg = config.modules.editors.vim;
  neovim-nightly = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/neovim-nightly-overlay/archive/master.tar.gz;
      # March 7th, 2021
      # sha256 = "001vsk9jnv2qca74a9frvl3jbygkf3h6jywbczaj9mnpn4zp9g2d";
    }))
  ];
in
{
  options = {
    modules.editors.vim = {
      enable = mkOption {
        default = false;
        type = with types; bool;
        description = "Enable Neovim nightly.";
      };
    };
  };

  config = mkIf cfg.enable {
    programs.neovim = {
      enable = true;
      package = pkgs.neovim-nightly;
    };
  };
}
