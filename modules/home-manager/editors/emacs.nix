let

  unstable = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/master.tar.gz") {
    overlays = [
      (import (builtins.fetchTarball {
        url = "https://github.com/nix-community/emacs-overlay/archive/850ddaaadd4c640bd8eb4b154734a96bbe234d07.tar.gz";
      }))
    ];
  };

  normal = import <nixpkgs> {
    overlays = [
      (import (builtins.fetchTarball {
        url = "https://github.com/nix-community/emacs-overlay/archive/master.tar.gz";
      }))
    ];
  };

in
{
  home.packages = with unstable; [

    (emacsWithPackagesFromUsePackage {
      config = ../../config/emacs/old_config/emacs-config.el;
      package = emacsGcc;
      alwaysEnsure = true;
      # alwaysTangle = true;
      extraEmacsPackages = epkgs: [
        emacs-all-the-icons-fonts
        epkgs.pdf-tools
        epkgs.org-pdftools
        epkgs.vterm
      ];
    })

  ];
}
