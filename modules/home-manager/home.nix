{ config, pkgs, lib, ... }:

{
  programs.home-manager.enable = true;
  fonts.fontconfig.enable = true;
  imports = [ ./cli
              ./editors
              ./fonts
              # ./languages
              ./scripts
              # ./mail
            ];

  home.packages = with pkgs; [
    nix
  ];

  xdg.enable = true;

  xdg.configFile."nix/nix.conf".text = ''
    experimental-features = nix-command flakes
    substituters = https://cache.nixos.org https://nix-community.cachix.org
    trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=
  '';
  # home.stateVersion = "20.09";
}
