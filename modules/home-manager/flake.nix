{
  description = "Home Manager Configuration";

  inputs = {

    nixpkgs = {
      url = "github:nixos/nixpkgs/nixpkgs-unstable";
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

  };

  outputs = { self, ... }@inputs:
  # inputs.flake-utils.lib.eachDefaultSystem (system:
  #   {
  #     legacyPackages = inputs.nixpkgs.legacyPackages.${system};
  #   }
  #   ) //
  {

    homeConfigurations = {
      macbook-pro = inputs.home-manager.lib.homeManagerConfiguration {
        system = "x86_64-darwin";
        username = "anthony";
        homeDirectory = "/Users/anthony";
        configuration = { pkgs, config, ... }:
        {
          programs.home-manager.enable = true;
          fonts.fontconfig.enable = true;
          xdg.configFile."nix/nix.conf".text = ''
            experimental-features = nix-command flakes
          '';
          imports = [
            ./cli
            ./editors
            ./fonts
            ./scripts
          ];
        };
    };
  };

};
}
