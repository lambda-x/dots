{ config, pkgs, lib, ... }:

{
  home.packages = with pkgs;
    [
      alegreya
      alegreya-sans
      cascadia-code
      eb-garamond
      etBook
      ibm-plex
      jetbrains-mono
      lato
      libertine
      libre-baskerville
      roboto-mono
      source-code-pro
      (nerdfonts.override {
        fonts = [ 
          "Hack"
          "Hasklig"
          "FiraCode"
          "IBMPlexMono"
          "Iosevka"
          "iA-Writer"
        ];
      })
    ];
}
