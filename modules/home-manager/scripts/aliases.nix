{ pkgs, config, ... }:

{
  programs.zsh.shellAliases = {
    rm-all = "rm -rfv ./{*,.*}";
  };
}
