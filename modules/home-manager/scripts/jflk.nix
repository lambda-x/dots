{ pkgs, config, ... }:

let

jflk = pkgs.writeScriptBin "jflk" ''

echo Choose project name:

read name

mkdir ~/source/js/$name

cd ~/source/js/$name

fs

'';

in
    {
      home.packages = [ jflk ];
      
      programs.zsh.shellAliases = {
        jflk = "source jflk";
      };
      
      programs.bash.shellAliases = {
        jflk = "source jflk";
      };
    }
