{ pkgs, config, ... }:

let
  fs = pkgs.writeScriptBin "fs" ''

cat <<'EOF' >> flake.nix
{
  description = "Initial Flake file";

  inputs = {

    flake-utils = {
      url = "github:numtide/flake-utils";
    };

    nixpkgs = {
      url = "nixpkgs/nixos-21.11";
    };

  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
        let
          pkgs = nixpkgs.legacyPackages.''${system};
        in {
          devShell = pkgs.mkShell {
EOF

cat <<EOF >> flake.nix
            buildInputs = with pkgs; [
             # Replace me
            ];
          };
        });
}
EOF

echo "use flake" >> .envrc && direnv allow

echo Finished!
'';

in

{
  home.packages = [ fs ];
}
