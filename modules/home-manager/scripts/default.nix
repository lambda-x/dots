{ pkgs, config, ... }:

{
  imports = [
    ./nsetup.nix
    ./fs.nix
    ./preprocess.nix
    ./a-shell.nix
    ./jflk.nix
    ./hmu.nix
    ./aliases.nix
  ];
}
