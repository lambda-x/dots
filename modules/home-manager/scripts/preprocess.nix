{ pkgs, config, ... }:

let

  preproc = pkgs.writeScriptBin "preproc" ''

#!{pkgs.stdenv.shell}
exec pdftotext - -
'';

in
    {
        home.packages = [ preproc ];
    }
