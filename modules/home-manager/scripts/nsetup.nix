{ pkgs, config, ... }:

let

nsetup = pkgs.writeScriptBin "nsetup" ''

#!{pkgs.stdenv.shell}

if [ -z $1 ]
then
    echo "Importing <nixpkgs>"
    pkgs="import <nixpkgs> {};"
else
    pkgs="import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/$1.tar.gz") {};"

fi

cat <<EOF >> shell.nix
let
  pkgs = $pkgs
in

with pkgs;

mkShell {
  buildInputs = [
  
  ];
}
EOF

echo "use_nix" >> .envrc && direnv allow

echo Finished!
'';

in
    {
        home.packages = [ nsetup ];
    }
