{ pkgs, config, ... }:

let

  hmu = pkgs.writeScriptBin "hmu" ''

home-manager switch --flake ~/dots/modules/#mbp

source ~/.config/zsh/.zshrc
'';

in
{
  home.packages = [ hmu ];

  programs.zsh.shellAliases = {
    hmu = "source hmu";
  };
}
