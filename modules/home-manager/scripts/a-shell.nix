{ pkgs, config, ... }:

let

ashell = pkgs.writeScriptBin "ashell" ''

#!{pkgs.stdenv.shell}

echo "Importing <nixpkgs>"
pkgs="import <nixpkgs> {};"

cat <<EOF >> shell.nix
let
  pkgs = $pkgs
in

with pkgs;

mkShell {
  buildInputs = [
    $1
    $2
  ];
}
EOF

echo "use_nix" >> .envrc && direnv allow

echo Finished!
'';

in
    {
        home.packages = [ ashell ];
    }
