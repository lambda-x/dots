{ pkgs, config, ... }:

{
  
  imports = [
    # ./haskell.nix
    ./javascript.nix
    ./dotnet.nix
  ];
  
}
