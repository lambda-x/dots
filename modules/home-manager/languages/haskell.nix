{ pkgs, ... }:
{
home.packages = with pkgs; [
  cabal2nix
  cabal-install
  haskellPackages.ghc-mod
  haskellPackages.ghcide
  haskellPackages.haskell-language-server
  ghc
];
}
