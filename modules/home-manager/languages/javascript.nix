{ pkgs, ... }:
{
home.packages = with pkgs; [
  nodejs
  deno
  nodePackages.typescript
  nodePackages.node2nix
  nodePackages.typescript-language-server
  nodePackages.prettier
  nodePackages.prettier_d
  nodePackages.create-react-app
  nodePackages.eslint_d
];
}
