{
  description = "Lambda-x Darwin Config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    darwin.url = "github:lnl7/nix-darwin/master";
    darwin.inputs.nixpkgs.follows = "nixpkgs";
    
    # home-manager-release.url = "github:nix-community/home-manager";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    emacs-overlay.url = "github:nix-community/emacs-overlay";
  };

  outputs = { self, darwin, nixpkgs, home-manager, emacs-overlay, ... }:
    let
      # This issue helped me solve the problems I was having:
      # https://github.com/nix-community/home-manager/issues/2954
      system = "aarch64-darwin";
      pkgs = import nixpkgs {
        inherit system;
        config = {
          allowUnfree = true;
        };
      };
      overlays = [ emacs-overlay.overlay ];
    in
      {

        homeConfigurations = {
          mbp = home-manager.lib.homeManagerConfiguration {
            # inherit system pkgs;
            pkgs = nixpkgs.legacyPackages.${system};
            modules = [
              ./home-manager/home.nix
              {
                home = {
                  username = "anthony";
                  homeDirectory = "/Users/anthony";
                  stateVersion = "22.05";
                  packages = with pkgs; [
                    emacsNativeComp
                  ];
                };
              }
            ];
          };
          
          nixos = home-manager.lib.homeManagerConfiguration {
            system = "x86_64-linux";
            username = "anthony";
            homeDirectory = "/home/anthony";
            configuration = { pkgs, config, ... }:
              {
                imports = [	                
                  ./home-manager/home.nix
                ];
	              nixpkgs.overlays = overlays;
                nixpkgs.config = { allowUnfree = true; };
                home.packages = with pkgs; [
		              emacsGcc
		              jetbrains.rider
                  jetbrains.idea-ultimate
                ];
              };
          };
        };

        nixosConfigurations.lenovo-nixos = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [ ./configuration.nix ];
        };

        darwinConfigurations."lambda-x" = darwin.lib.darwinSystem {
          system = "aarch64-darwin";
          modules = [
            ./darwin-configuration.nix
          ];
        };

      };
}
