{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./cachix.nix
    ];
  
  boot = {
    loader = {
      
      grub = {
        enable = true;
        version = 2;
        efiSupport = true;
        device = "nodev";
      };

      systemd-boot = {
        enable = true;
      };

      efi = {
        canTouchEfiVariables = true;
      };

    };

   kernelModules = [ "kvm-amd" "kvm-intel" ];
  };

  networking.hostName = "Lenovo-Desktop";

  time.timeZone = "America/Chicago";

  networking.useDHCP = false;
  networking.interfaces.eno1.useDHCP = true;

  i18n.defaultLocale = "en_US.UTF-8";

  services = {
    
    printing = {
      enable = true;
      drivers = [ pkgs.hplip ];
    };

    xserver = {
      enable = true;
      layout = "us";
      # xkbOptions = "";

      desktopManager = {
        plasma5 = {
          enable = true;
        };
      };

      displayManager = {
        sddm = {
          enable = true;
        };
      };

    };
  };

  hardware.cpu.intel.updateMicrocode = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  users = {
    users = {
      anthony = {
        isNormalUser = true;
        extraGroups = [
          "wheel"
          "audio"
          "vboxusers"
        ];
        initialPassword = "pass";
      };
    };
  };

  programs.zsh.enable = true;

  security.sudo.wheelNeedsPassword = false;

  environment.systemPackages = with pkgs; [
    wget vim
    firefox git
  ];

  console.font =
    "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";

  virtualisation = {
    virtualbox = {
      host = {
        enable = true;
      };
    };
    libvirtd = {
        enable = true;
    };
  };

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  system.stateVersion = "20.09"; # Did you read the comment?

}
