{ config, pkgs, ... }:
{
  # imports = [ ./cachix.nix ];

  environment.systemPackages = with pkgs;
  [ vim
  ];

  system.defaults = {

    dock = {
      autohide    = true;
      launchanim  = false;
      orientation = "right";
    };

    NSGlobalDomain = {
      NSAutomaticCapitalizationEnabled = false;
      NSAutomaticDashSubstitutionEnabled = false;
      NSAutomaticPeriodSubstitutionEnabled = false;
      NSAutomaticSpellingCorrectionEnabled = false;
    };

    trackpad = {
      Clicking = true;
      TrackpadThreeFingerDrag = true;
    };

  };

  nix.settings.trusted-users = [ "anthony" "@admin" ];
  nix.settings.substituters = [ "https://nix-community.cachix.org" ];
  nix.settings.trusted-public-keys = [ "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=" ];

  nix.package = pkgs.nixUnstable;

  nix.extraOptions = "experimental-features = nix-command flakes";

  homebrew = {
    enable = true;
    brews = [ "trash" ];
    casks = [
      "adobe-acrobat-reader"
      "alacritty"
      "aldente"
      "alfred"
      "bitwarden"
      "djview"
      "docker"
      "dropbox"
      "hammerspoon"
      "iterm2"
      "karabiner-elements"
      "microsoft-auto-update"
      "microsoft-edge"
      "spotify"
      "the-unarchiver"
      "vmware-fusion"
      "zotero"
    ];
  };

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  services.emacs.enable = true;

  # Create /etc/bashrc that loads the nix-darwin environment.

  programs = {

    bash = {
      enable = true;
      enableCompletion = true;
    };

    zsh = {
      enable = true;
      enableSyntaxHighlighting = true;
    };

  };

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}
